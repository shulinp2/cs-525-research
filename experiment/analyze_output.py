import re
import matplotlib.pyplot as plt
import datetime

def parse_output_model(model_name):
    # initialize variables
    rounds = [[1,2,3,4,5,6,7,8,9,10] for _ in range(1, 10)]
    times = [[] for _ in range(1, 10)]
    losses = [[] for _ in range(1, 10)]
    training_times = [[] for _ in range(1, 10)]
    for worker_id in range(1,10):
        # open the log file
        print(f'{model_name}_baseline_output/output_{model_name}_worker{worker_id}.txt')
        with open(f'{model_name}_baseline_output/output_{model_name}_worker{worker_id}.txt', 'r') as f:
            for line in f:
                # check if line contains round_id information
                # match = re.search(r'round_id = (\d+)', line)
                # if match:
                #     round_id = int(match.group(1))
                # check if line contains client training time information
                match = re.search(r'client (\d+)\s+training time:\s+([\d.]+)', line)
                if match:
                    client_id = int(match.group(1))
                    training_time = float(match.group(2))
                    training_times[worker_id - 1].append(training_time)
                    # print(f"Round {round_id}, Client {client_id}, Training time: {training_time}s")
                
                match = re.search(r'^\s*\-\s*([\w\s,:]+)\s+', line)
                if match:
                    # print("there is a match")
                    timestamp_str = match.group(1)
                    # convert timestamp string to datetime object
                    dt_object = datetime.datetime.strptime(timestamp_str, '%a, %d %b %Y %H:%M:%S')
                    # convert datetime object to seconds since epoch
                    timestamp_seconds = dt_object.timestamp()
                    match = re.search(r'Epoch: (\d+)\s+Loss: ([\d.]+)', line)
                    if match:
                        epoch = int(match.group(1))
                        loss = float(match.group(2))
                        losses[worker_id - 1].append(loss)
                        times[worker_id - 1].append(timestamp_seconds)
    
    # plot training time
    print(len(training_times))
    fig, ax = plt.subplots()
    for i in range(1,10):
        ax.plot(rounds[i - 1], training_times[i - 1],label=f"Worker {i}")
    ax.set_xlabel('Rounds')
    ax.set_ylabel(f'{model_name} Training Time')
    ax.set_title('Training time for each rounds of 9 workers')
    ax.legend()
    plt.show()
    # plot the losses over time
    fig2, ax2 = plt.subplots()
    for i in range(1,10):
        ax2.plot(times[i - 1], losses[i - 1], label=f"Worker {i}")
    ax2.set_xlabel('Timestamp (seconds)')
    ax2.set_ylabel(f'{model_name} Loss')
    ax2.set_title('Loss over time for 9 workers')
    ax2.legend()
    plt.show()


if __name__ == '__main__':
    parse_output_model("cifar")
    parse_output_model("mnist")
    parse_output_model("fashion_mnist")